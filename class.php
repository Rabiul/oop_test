<?php
/*class Car {
  public $comp;
  public $color = 'beige';
  public $hasSunRoof = true;

  public function hello(){
  	return "Hello";
  }
}

$bmw = new Car ();
echo $bmw->color="Green"."<br>";
echo $bmw->hello()."<br>";


$mercidise = new Car(); 
echo $mercidise->comp="Samsung"; */






/*class Car {

    // The properties
    public $comp;
    public $color = 'beige';
    public $hasSunRoof = true;
 
    // The method can now approach the class properties
    // with the $this keyword
    public function hello()
    {
      return "Beep I am a <i>" . $this -> comp . "</i>, and I am <i>" .
        $this -> color.$this ->hasSunRoof;
    }
}

$hi = new Car();
echo $hi->hello(); */







/*class Car {
 
  public $tank;
  
  // Add gallons of fuel to the tank when we fill it.
  public function fill($float)
  {
    $this-> tank += $float;
 
    return $this;
  }
  
  // Substract gallons of fuel from the tank as we ride the car.
  public function ride($float)
  {
    $miles = $float;
    $gallons = $miles/50;
    $this-> tank -= ($gallons);
 
    return $this;
  }
}
// Create a new object from the Car class.
$bmw = new Car();
 
// Add 10 gallons of fuel, then ride 40 miles, 
// and get the number of gallons in the tank. 
$tank = $bmw -> fill(10) -> ride(50) -> tank;
 
// Print the results to the screen.
echo "The number of gallons left in the tank: " . $tank . " gal.";*/




class Car {
 
  //the private access modifier denies access to the method from outside the class’s scope
  private $modeli;
 
  //the public access modifier allows the access to the method from outside the class
  public function setModel($modeli)
  {
    $this -> model = $modeli;
  }
  
  public function getModel()
  {
    return "The car model is  " . $this -> model;
  }
}
 
$mercedes = new Car();
//Sets the car’s model
$mercedes -> setModel("Mercedes benz");
//Gets the car’s model
echo $mercedes -> getModel();


?> 